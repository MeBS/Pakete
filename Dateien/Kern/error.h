#ifndef _ERROR_H
#define _ERROR_H
#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>

static void error(int exitValue, int num, const char* f, ...)
{
	va_list va;
	va_start(va, f);
	vfprintf(stderr, f, va);
	va_end(va);
	if(num != 0)
	{
		fprintf(stderr, ": %i", num);
	}
	fprintf(stderr, "\n");
	if(exitValue)
	{
		exit(exitValue);
	}
}

#endif
